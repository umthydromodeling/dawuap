Contributing to daWUAP
======================

*Rules and guidelines for Pull Requests, etc. are forthcoming*


For Maintainers
---------------

**Updating the Python Package Index (PyPI)**

[Reference this documentation on Python.org for more details.](https://packaging.python.org/tutorials/packaging-projects/#generating-distribution-archives) Before getting started, make sure you have:

- Installed the necessary Python packages: `pip install setuptools wheel twine`
- Created an API key on PyPI to use for this project: `https://pypi.org/manage/account/`

To update the daWUAP package on PyPI, there are only two simple steps!

```
# Package everything into files in the dist/ folder
python setup.py sdist bdist_wheel

# Upload to TestPyPI
python -m twine upload --repository pypi dist/*VERSION*
```

[Check out the changes at PyPI.](https://pypi.org/project/daWUAP/)

**Note that `*VERSION*` above should be changed to match the specific version of daWUAP you want to upload** (there may be many versions in the `dist/` folder).
