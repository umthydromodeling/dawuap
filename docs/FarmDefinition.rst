Input file formats
====================

Farm definition file
--------------------

The farm definition file contains the following information for each land unit
(`farm`):

* `id`: the id of the unit (`farm`), to relate with the id's in the geoJSON file
* `name`: the name of the unit (`farm`)
* `source_i`: the id of the node of the hydrological glyph where the water is
  extracted from (diversion point); it provides a link with the hydrological part
* `irrigation_eff`: irrigation efficiency per crop (the ratio between the volume of
  water diverted from the source and the water really applied in the fields); a
  value between 0 and 1
* `input_list`: a list of the inputs (currently it always contains 'land' and
  'water' and it is not changed)
* `irrigation_mask`: a boolean vector indicating which crops are irrigated
* `crop_list`: the names of the crops
* `crop_id`: the id's of the crops linking to table `utils/crop_coefficients.txt`;
  this table contains, for each crop, crop coefficient for different crop stages;
  this information is used for computing the crop evapotranspiration when linked
  with the hydrological module
* `simulated_states`: a place to store the simulated results, initally set to all
  zeros; childs are:

 * `shadow_price_land`:
 * `net_revenues`:
 * `yields`:
 * `supply_elasticity_eta`:
 * `used_water`:
 * `yield_elasticity_water`:
 * `used_land`:
 * `shadow_price_water`:

* `parameters`: parameters of the production and cost functions, which need to be
  calibrated for each crop:

  * `deltas`:
  * `mus`:
  * `lambdas_lan`:
  * `first_stage_lambda`:
  * `betas`:
  * `sigma`:

* `normalization_refs`: reference crop values which are used for normalizing
  (i.e., historical mean values)

  * `reference_yields`: mean historical yield of each crop
  * `reference_et`: mean historical applied water (precipitation plus irrigation)
  * `reference_land`: maximum historical total land put under production,
      (this is the theoretical maximum amount of land that could be allocated to
      a given crop, so it is replicated times the number of crops)
  * `reference_prices`: mean historical crop prices
  * `constraints`: constraints; this is not used and it will be deprecated


Observations Definition File
----------------------------

The observations file contains the following information for each land unit (`farm`):

* `id`: the id of the unit (`farm`), to relate with the id's in the geoJSON file
* `name`: the name of the unit (`farm`)
* `year`: the year of the observation
* `crop_list`: the list of crops; its order and number must be the same found in the
  farm specification file
* `prices`: the observed crop prices for the year
* `costs`: the best approximation of the costs of cultivating one extra unit of land
  and water, per crop; there are two columns, one for land and one for water
* `obs_water`: the observed water received per crop (i.e. the sum of precipitated
  and irrigation water, in mm; for rainfed crops, thus, this is only the precipitation)
* `obs_land`: the observed land allocation per crop
* `ybar`: observed production (yield * acreage), per crop
* `ybar_w`: water supply elasticity: the relationship between production and water
  supply (it can be computed by OLS regression between the log(ybar) and log(obs_water),
  using data from a number of years, maybe from a pool of regions)
* `eta`: crop supply elasticity: the relationship between crop production and crop
  prices (it can be computed by OLS regression between log(ybar) and log(prices), or
  from national statistics)

Scenario definition file
------------------------

## Simulate a baseline scenario

We are going to make a simulation with the same conditions observed for year 2012. The goal is to check if the calibrated parameters yield a results close to the observation.

The scenarios file contain the following data for each land unit (`farm`):

* `farm_id`: the id of the county (`farm`), to relate with the id's in the
   geo_json file
* `year`: the year of the simulation; only used as a reference
* `prices`: the market prices of the crops
* `costs`: the costs of cultivating one extra unit of land and water, per crop
  (a linear cost model is used); there are two columns, one for land and one for
  water
* `crop_start_date`: planting date for each crop
* `crop_cover_date`: date of maximum crop development, for each crop
* `crop_end_date`: date of harvest, for each crop
* `land_constraint`: maximum available total land (a fraction of the land might
  be left to fallow due to normal rotation, for instance; usually, this the
  land allocation of the year with the largest value is used)
* `water_contraint`: maximum available water for irrigation (mm)
* `evapotranspiration`: volume of rainfall water used by each crop on the year
  (mm); useful for simulating climatic droughts; its maximum value can be
  calculated as the ETo times the crop factor (crop evapotranspiration)

Note: In our case, we could set the `evapotranspiration` equal to the total
season precipitation (i.e., for winter wheat it could be from the previous
harvest to maturity, while for the second crop--corn--it could be left to
the precipitation between july and november).

The simulations file has the same structure as the farm specification file,
but contains the results of the simulation under the `simulated_states` child,
with the following data:

* `simulated_states`:

 * `yields`: the production (not yields!) of each crop, for the simulation
      year
 * `net_revenues`: the net revenues per crop (that is, once the costs have
   been removed)
 * `used_land`: the land allocated to each crop
 * `used_water`: the irrigation water applied to each crop
 * `shadow_price_land`: the shadow price (opportunity cost) of land
 * `shadow_price_water`: the shadow price (opportunity cost) of water
 * `supply_elasticity_eta`: rate of change of crop production with respect
   to the rate of change of crop prices: ((\delta price) \over (price)) \over
   ((\delta yield) \over (yield)); how much the market prices is controlled by
   crop production; this can be inverted so we learn about how much the
   production is influenced by the prices
 * `yield_elasticity_water`: rate of change of crop production with respect
   to the rate of change of water applied: ((\delta yield) \over (yield)) \over
   ((\delta water) \over (water)); how much the water use is influenced by the
   crop production, which can also be inverted
