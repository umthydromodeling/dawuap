.. hydroengine documentation master file, created by
   sphinx-quickstart on Tue Jun 27 08:48:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HyEnA
================
A Hydro-Economic Model of Agricultural Productivity in Python

Purpose
-------

HyEnA is a hydro-economic model developed in pure Python to investigate imbalances in the water supply (hydrology) and water demands (agriculture) systems.

Economic component
------------------
The two components


Quick Start
-----------

Economic module using standard PMP
-------------------------------------
Input file formats
------------------
.. toctree::
   :maxdepth: 4
   :caption: Contents:

   FarmDefinition


Economic module using Bayesian recursive estimation
---------------------------------------------------
Input file formats
------------------
.. toctree::
   :maxdepth: 4
   :caption: Contents:

   FarmDefinition


Technical module documentation
---------------------------------------------------

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   hydroengine
   utils
   example


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
