import glob
import hashlib
import json
import pickle
import os
import unittest
import numpy as np
import daWUAP
import rasterio
from affine import Affine
from daWUAP.assimilation.kalmanfilter import KalmanFilter
from daWUAP.econengine import Farm
from daWUAP.utils.fixtures import generate_farm, generate_observation
from daWUAP.utils.check_farms import check_farms

TEST_DATA_DIR = os.path.join(os.path.dirname(daWUAP.__file__), 'tests/data')

class CalibrationSuite(unittest.TestCase):
    '''
    Suite of tests related to calibrating farmer behavior based on
    observations.
    '''
    _kf_output_files = [
        '%s/30063_%s' % (TEST_DATA_DIR, s)
        for s in ('innovation.csv', 'kf_info.json', 'parameter_ensemble.csv')
    ]

    @classmethod
    def setUpClass(cls):
        with open('%s/Farms.json' % TEST_DATA_DIR, 'r') as file:
            cls._farms = json.load(file)
        with open('%s/Observations.json' % TEST_DATA_DIR, 'r') as file:
            cls._observations = json.load(file)

    @classmethod
    def tearDownClass(cls):
        for filename in cls._kf_output_files:
            if os.path.exists(filename):
                os.remove(filename)

    def test_farm_instance(self):
        'Tests that a Farm instance can be instantiated'
        for farm_data in self._farms['farms']:
            Farm(**farm_data)

    def test_write_farm_dict(self):
        'Farm instance should correctly write_farm_dict()'
        for farm_data in self._farms['farms']:
            f = Farm(**farm_data)
            check_farms(f.write_farm_dict())

    def test_calibration_pmp(self):
        'Farm calibration with PMP should work as expected'
        farm = Farm(**self._farms['farms'][0], verbose = False)
        obs = list(filter(lambda d: d['id'] == farm.id, self._observations)).pop()
        result = farm.calibrate(**obs)
        self.assertTrue(result.success)
        # Values of objective function should be small
        self.assertTrue(result.fun.sum() < 1e-10)
        self.assertTrue(np.equal(farm.deltas.round(2),
            [0.43, 0.27, 0.26, 0.26, 0.28, 0.28, 0.26]).all())
        # Betas for each crop should sum to 1.0
        self.assertTrue(np.equal(farm.betas.sum(axis = 1).round(1), 1).all())
        self.assertTrue(np.equal(farm.first_stage_lambda.round(2)[0], 0.25))
        self.assertTrue(np.equal(farm.mus.round(2),
            [0.5 , 0.18, 0.02, 0.03, 0.03, 0.03, 0.04]).all())

    def test_calibration_stochastic_spin_up(self):
        'Test stochastic data assimilation and output file creation'
        farm = Farm(**self._farms['farms'][0], verbose = False)
        obs = list(filter(lambda d: d['id'] == farm.id, self._observations)).pop()
        kf = KalmanFilter(farm, 10, xi = 0.50, cv = 3.0)
        for i in range(0, 2):
            if i > 0:
                kf = KalmanFilter(
                    farm, fn_info_file = os.path.join(TEST_DATA_DIR, '%s_kf_info.json' % str(farm.id)))
            foo = kf.assimilate(obs, obs_var_scale_factor = 1)
            kf.save_kalman_filter(TEST_DATA_DIR, timestep_label = str(i))
        for filename in self._kf_output_files:
            self.assertTrue(os.path.exists(filename))


if __name__ == '__main__':
    unittest.main()
