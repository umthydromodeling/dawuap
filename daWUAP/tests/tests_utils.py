import csv
import glob
import json
import os
import unittest
import numpy as np
import rasterio
import daWUAP
from affine import Affine
from rasterio.profiles import DefaultGTiffProfile
from daWUAP.utils import NumpyEncoder
from daWUAP.utils.vector import VectorDataset, ModelVectorDatasets, NetworkParser
from daWUAP.utils.raster import RasterDataset, ModelRasterDatasetHBV
from daWUAP.utils.io import MAP_OBS_JSON_TO_CSV, MAP_FARM_JSON_TO_CSV, observations_json_to_csv, observations_csv_to_json, farms_json_to_csv, farms_csv_to_json
from daWUAP.utils.check_farms import check_farms
from daWUAP.utils.fixtures import generate_farm

TEST_DATA_DIR = os.path.join(os.path.dirname(daWUAP.__file__), 'tests/data')

class DataGeneration(unittest.TestCase):
    '''
    Suite of unit tests for generating Farm, Scenario, or Observation data.
    '''
    def test_generate_farm(self):
        'Test that Farm data are generated correctly'
        farm = generate_farm(3)
        check_farms(farm)


class InputOutput(unittest.TestCase):
    'Test cases of input/output functions (io.py)'
    _output_csv_path = os.path.join(TEST_DATA_DIR, 'temp.csv')

    @classmethod
    def setUpClass(cls):
        # Note that that the value for each crop-specific parameter is just
        #   one of a sequence of increasing integers
        crops = ['Pineapple', 'Guanabana', 'Avocado']
        # Build the Farms dictionary
        cls._farms_dict = {'farms': list()}
        cls._farms_dict['farms'].append(dict(
            (k, [i for i in range(0, len(crops))])
            for k in MAP_FARM_JSON_TO_CSV.keys()
        ))
        cls._farms_dict['farms'][0]['id'] = '99999'
        cls._farms_dict['farms'][0]['name'] = 'Test'
        cls._farms_dict['farms'][0]['source_id'] = 42
        cls._farms_dict['farms'][0]['crop_id'] = list(range(0, len(crops)))
        cls._farms_dict['farms'][0]['crop_list'] = crops
        cls._farms_dict['farms'][0]['input_list'] = ['land', 'water']
        cls._farms_dict['farms'][0]['constraints'] = {
            'land': [-1], 'water': [-1]
        }
        cls._farms_dict['farms'][0]['normalization_refs'] = dict(
            (k, [float(i) for i in range(0, len(crops))])
            for k in MAP_FARM_JSON_TO_CSV['normalization_refs'].keys()
        )
        cls._farms_dict['farms'][0]['simulated_states'] = dict(
            (k, np.float32(42)) if k in ('shadow_prices', 'net_revenues') else (k, [
                i for i in range(0, len(crops))
            ])
            for k in MAP_FARM_JSON_TO_CSV['simulated_states'].keys()
        )
        cls._farms_dict['farms'][0]['parameters'] = dict(
            (k, [(-1, 1) for c in (range(0, len(crops)))])
            for k in ('lambdas_land', 'betas')
        )
        cls._farms_dict['farms'][0]['parameters']['deltas'] = [
            float(i) for i in range(0, len(crops))
        ]
        cls._farms_dict['farms'][0]['parameters']['mus'] = [
            float(i) for i in range(0, len(crops))
        ]
        cls._farms_dict['farms'][0]['parameters']['first_stage_lambda'] = [np.float32(42)]
        cls._farms_dict['farms'][0]['parameters']['sigmas'] = [np.float32(42)]
        # Build the Observations dictionary
        cls._obs_dict = dict(
            (k, [i for i in range(0, len(crops))])
            for k in MAP_OBS_JSON_TO_CSV.keys()
        )
        cls._obs_dict['year'] = 1999
        cls._obs_dict['id'] = '99999'
        cls._obs_dict['name'] = 'Test'
        cls._obs_dict['crop_list'] = crops
        cls._obs_dict.update(list(
            [k, [[-1, 1] for i in range(0, len(crops))]]
            for k in ('mean_costs', 'std_costs')
        ))

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls._output_csv_path):
            os.remove(cls._output_csv_path)

    def test_farms_json_to_csv_exception(self):
        'JSON-to-CSV serialization should raise exception if non-dict supplied'
        with self.assertRaises(NotImplementedError) as exception:
            farms_json_to_csv(self._farms_dict['farms'], self._output_csv_path)

    def test_farms_csv_to_json(self):
        'JSON-to-CSV serialization should be reversible'
        farms_json_to_csv(self._farms_dict, self._output_csv_path)
        result = farms_csv_to_json(self._output_csv_path)
        self.maxDiff = None
        self.assertEqual(
            json.dumps(self._farms_dict, sort_keys = True, cls = NumpyEncoder),
            json.dumps(result, sort_keys = True, cls = NumpyEncoder))

    def test_farms_json_integrity(self):
        'Farms dictionary thus converted should match expectations'
        check_farms(self._farms_dict)
        result = farms_csv_to_json(self._output_csv_path)
        check_farms(result)

    def test_observations_json_to_csv_exception(self):
        'JSON-to-CSV serialization should raise exception if non-list supplied'
        with self.assertRaises(NotImplementedError) as exception:
            observations_json_to_csv(self._obs_dict, self._output_csv_path)

    def test_observations_csv_to_json(self):
        'JSON-to-CSV serialization should be reversible'
        observations_json_to_csv([self._obs_dict], self._output_csv_path)
        result = observations_csv_to_json(self._output_csv_path)
        self.assertEqual(
            json.dumps([self._obs_dict], sort_keys = True),
            json.dumps(result, sort_keys = True))


class RasterUtilities(unittest.TestCase):
    'Suite of raster utilities tests'
    _synth_precip_raster = os.path.join(TEST_DATA_DIR, 'precip.tiff')
    _synth_precip_profile = {
        'dtype': 'float32', 'nodata': -32767.0, 'width': 83, 'height': 49,
        'count': 3, 'crs': None, 'tiled': False,
        'transform': Affine(
            0.0416, 0.0, -115.7458333, 0.0, -0.0416, 47.50416),
        }
    _output_param_raster = os.path.join(TEST_DATA_DIR, 'rparam.tiff')
    _output_temp_thresh_raster = os.path.join(TEST_DATA_DIR, 'temp_thresh.tiff')
    _output_paths = [
        _output_param_raster, _output_temp_thresh_raster, _synth_precip_raster
    ]

    @classmethod
    def setUpClass(cls):
        # Create synthetic data
        np.random.seed(9)
        profile = DefaultGTiffProfile(cls._synth_precip_profile)
        shp = (profile['height'], profile['width'])
        with rasterio.open(cls._synth_precip_raster, 'w', **profile) as out:
            for b in range(0, profile['count']):
                arr = np.random.lognormal(size = shp[0] * shp[1])\
                    .astype(rasterio.float32).reshape(shp)
                out.write(arr, b + 1)

    @classmethod
    def tearDownClass(cls):
        for path in cls._output_paths:
            if os.path.exists(path):
                os.remove(path)

    def test_raster_parameter_constant_value_write(self):
        'Should write out constant parameter arrays based on input raster'
        raster_file = RasterDataset(self._synth_precip_raster)
        dset = ModelRasterDatasetHBV(
            self._synth_precip_raster, self._output_temp_thresh_raster)
        dset.write_parameter_to_geotiff(self._output_temp_thresh_raster, 9)
        with rasterio.open(self._output_temp_thresh_raster, 'r') as rast:
            # Raster array should be "9.0" everywhere
            arr = rast.read()
            self.assertTrue(np.equal(9, arr.mean()))
            self.assertTrue(np.equal(9 * arr.size, arr.sum()))

    def test_raster_parameter_array_value_write(self):
        'Should write out parameter arrays based on input raster'
        raster_file = RasterDataset(self._synth_precip_raster)
        dset = ModelRasterDatasetHBV(
            self._synth_precip_raster, self._output_temp_thresh_raster)
        # Create an input array based on the base raster
        profile = DefaultGTiffProfile(self._synth_precip_profile)
        shp = (profile['height'], profile['width'])
        input_arr = np.arange(0, (shp[0] * shp[1])).reshape(shp)
        dset.write_parameter_to_geotiff(
            self._output_temp_thresh_raster, input_arr)
        with rasterio.open(self._output_temp_thresh_raster, 'r') as rast:
            # Raster array should match the array we wrote to the file
            self.assertTrue(np.equal(rast.read(), input_arr).all())


class VectorUtilities(unittest.TestCase):
    'Suite of vector utilties tests'
    _network_shp = os.path.join(TEST_DATA_DIR, 'rivers.shp')

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_network_connectivity_matrix(self):
        "Should read in and correctly parse a network file's connectivity"
        parser = NetworkParser(self._network_shp)
        self.assertTrue(np.equal(parser.conn_matrix.shape, (5, 5)).all())
        self.assertTrue(np.equal(parser.conn_matrix.sum(), [1, 2, 0, 1, 0]).all())

    def test_network_get_parameter(self):
        "Should access the values of a named parameter for all features"
        parser = NetworkParser(self._network_shp)
        self.assertEqual(parser.get_parameter('AREAC').sum(), 5664700.0)


class ModelVectorInitialization(unittest.TestCase):
    'Suite of tests for setting up a model run with vector data'

    _basins_shp = os.path.join(TEST_DATA_DIR, 'subbasins.shp')
    _rivers_shp = os.path.join(TEST_DATA_DIR, 'rivers.shp')
    _output_basins_shp_params = os.path.join(
        TEST_DATA_DIR, 'subbasins_with_params.shp')
    _output_rivers_shp_params = os.path.join(
        TEST_DATA_DIR, 'rivers_with_params.shp')

    @classmethod
    def tearDownClass(cls):
        # Output Shapefiles have a lot of component files...
        output_files = glob.glob(
            '.'.join(cls._output_basins_shp_params.split('.')[:-1]) + '*')
        output_files.extend(glob.glob(
            '.'.join(cls._output_rivers_shp_params.split('.')[:-1]) + '*'))
        for filename in output_files:
            os.remove(filename)

    def test_basins_HBV_parameter_update(self):
        'Should correctly write the HBV parameters to output vector dataset'
        basins = ModelVectorDatasets(
            fn_subsheds = self._basins_shp, fn_network = self._rivers_shp,
            verbose = False)
        params_hbv = []
        for sub in basins.subsheds.read_features():
            params_hbv.append({
                'SUBBASIN': sub['properties']['SUBBASIN'],
                'hbv_ck0': 12, 'hbv_ck1': 50, 'hbv_ck2': 10000, 'hbv_hl1': 50,
                'hbv_perc': 2, 'hbv_pbase': 5
            })
        basins.write_hbv_parameters(
            outfn = self._output_basins_shp_params, params = params_hbv)
        outfile = VectorDataset(self._output_basins_shp_params)
        self.assertTrue(np.equal([
            f['properties']['hbv_ck0'] for f in outfile._read_fiona_object()
        ], 12).all())

    def test_network_Muskingum_Cunge_parameter_update(self):
        'Should correctly write the Muskingum-Cunge parameters to output'
        basins = ModelVectorDatasets(
            fn_subsheds = self._basins_shp, fn_network = self._rivers_shp,
            verbose = False)
        params_musk = []
        for riv in basins.network.read_features():
            params_musk.append({
                'ARCID': riv['properties']['ARCID'], 'e': 0.3, 'ks': 86400
            })
        basins.write_muskingum_parameters(
            outfn = self._output_rivers_shp_params, params = params_musk)
        outfile = VectorDataset(self._output_rivers_shp_params)
        self.assertTrue(np.equal([
            f['properties']['ks'] for f in outfile._read_fiona_object()
        ], 86400).all())


if __name__ == '__main__':
    unittest.main()
