import unittest
from daWUAP.tests.tests_utils import DataGeneration, InputOutput, RasterUtilities, VectorUtilities, ModelVectorInitialization
from daWUAP.tests.tests_hydro import HydrologyEngine, HydrologyModel
from daWUAP.tests.tests_econ import CalibrationSuite

if __name__ == '__main__':
    unittest.main()
