import glob
import hashlib
import json
import pickle
import os
import unittest
import numpy as np
import daWUAP
import rasterio
from affine import Affine
from rasterio.profiles import DefaultGTiffProfile
from daWUAP.hyena import HydroEconModel
from daWUAP.hydroengine import Routing
from daWUAP.utils.vector import ModelVectorDatasets, NetworkParser, VectorDataset
from daWUAP.utils.raster import ModelRasterDatasetHBV, RasterDataset

TEST_DATA_DIR = os.path.join(os.path.dirname(daWUAP.__file__), 'tests/data')

class HydrologyEngine(unittest.TestCase):
    '''
    Unit tests for the hydroengine.Routing class.
    '''
    _conn = np.zeros((7, 7))
    _conn[0, 2] = 1
    _conn[1, 2] = 1
    _conn[2, 3] = 1
    _conn[3, 4] = 1
    _conn[3, 5] = 1
    _conn[4, 5] = 1
    _conn[5, 6] = 1

    @classmethod
    def setUpClass(cls):
        cls._route = Routing(cls._conn, 0.5)
        K = np.ones(cls._route.n) * 0.7
        K[2] = 0.6
        e = np.ones_like(K) * 0.42
        Qk = np.ones_like(K) * 0.2
        cls._params = (Qk, K, e)

    def test_routing_initialization(self):
        'Test initialization of the Routing instance'
        self.assertTrue(np.equal(self._conn, self._route.conn).all())
        self.assertEqual(self._route.n, np.shape(self._conn)[0])

    def test_routing_without_inflows_or_diversions(self):
        'Routing without inflows or diversions should yield expected flows'
        Qk1 = self._route.muskingum_routing(*self._params, 0, 0, 0)
        self.assertEqual(Qk1.size, 7)
        self.assertEqual(Qk1.sum().round(3), 1.387)
        self.assertEqual(Qk1[0].round(3), 0.09)

    def test_routing_with_inflows(self):
        'Routing with inflows, no diversions, should yield expected flows'
        Qk1 = self._route.muskingum_routing(*self._params, 0.1, 0, 0)
        self.assertEqual(Qk1.size, 7)
        self.assertEqual(Qk1.sum().round(3), 1.606)
        self.assertEqual(Qk1[0].round(3), 0.112)

    def test_routing_with_diversions(self):
        'Routing with diversions, no inflow, should yield expected flows'
        Qk1 = self._route.muskingum_routing(*self._params, 0, 0, 0.05)
        self.assertEqual(Qk1.size, 7)
        self.assertEqual(Qk1.sum().round(3), 1.13)
        self.assertEqual(Qk1[0].round(3), 0.062)

    def test_routing_with_inflows_and_diversions(self):
        'ROuting with inflows and diversions should yield expected flows'
        Qk1 = self._route.muskingum_routing(*self._params, 0.1, 0, 0.15)
        self.assertEqual(Qk1.size, 7)
        self.assertEqual(Qk1.sum().round(3), 0.834)
        self.assertEqual(Qk1[0].round(3), 0.03)


class HydrologyModel(unittest.TestCase):
    '''
    Suite of hydrology model tests. Runs a full hydrology model with synthetic
    data (randomly generated but with a fixed random seed), then checks that
    the output files are identical to a past run using SHA-256 checksums for
    binary files and with more substantial tests for output text files.
    '''
    _synth_precip_raster = os.path.join(TEST_DATA_DIR, 'precip.tiff')
    _synth_tmax_raster = os.path.join(TEST_DATA_DIR, 'tmax.tiff')
    _synth_tmin_raster = os.path.join(TEST_DATA_DIR, 'tmin.tiff')
    _synth_profile = {
        'dtype': 'float32', 'nodata': -32767.0, 'width': 83, 'height': 49,
        'count': 3, 'crs': None, 'tiled': False,
        'transform': Affine(
            0.0416, 0.0, -115.7458333, 0.0, -0.0416, 47.50416),
        }
    _output_hydro_rasters = [
        'pp_temp_thresh.tif', 'ddf.tif', 'soil_max_water.tif', 'soil_beta.tif',
        'aet_lp_param.tif'
    ]
    _output_paths = _output_hydro_rasters[:]
    _output_paths.extend([
        'rivers_with_params.%s' % ext
        for ext in ('cpg', 'dbf', 'prj', 'shp', 'shx')
    ])
    _output_paths.extend([
        'subbasins_with_params.%s' % ext
        for ext in ('cpg', 'dbf', 'prj', 'shp', 'shx')
    ])
    _output_paths.extend(('param_files_test.json',))
    _output_paths = list(map(
        lambda p: os.path.join(TEST_DATA_DIR, p), _output_paths))
    _output_paths.extend(
        (_synth_precip_raster, _synth_tmin_raster, _synth_tmax_raster))

    @classmethod
    def setUpClass(cls):
        # Create synthetic data
        np.random.seed(9)
        profile = DefaultGTiffProfile(cls._synth_profile)
        shp = (profile['height'], profile['width'])
        # Precip: Fixed the mean and std. deviation parameters from real data
        values = np.random.lognormal(0.811, 1.14, size = shp[0] * shp[1])
        with rasterio.open(cls._synth_precip_raster, 'w', **profile) as out:
            for b in range(0, profile['count']):
                arr = values.astype(rasterio.float32).reshape(shp)
                out.write(arr, b + 1)
        # Tmin: Fixed the mean and std. deviation parameters from real data
        values = np.random.normal(271, 7.5, size = shp[0] * shp[1])
        with rasterio.open(cls._synth_tmin_raster, 'w', **profile) as out:
            for b in range(0, profile['count']):
                arr = values.astype(rasterio.float32).reshape(shp)
                out.write(arr, b + 1)
        # Tmax: Fixed the mean and std. deviation parameters from real data
        size = shp[0] * shp[1]
        values0 = np.random.normal(275, 7.5, size = int(0.6 * size))
        values1 = np.random.normal(298, 7.5, size = size - values0.size)
        values = np.concatenate((values0, values1))
        np.random.shuffle(values)
        with rasterio.open(cls._synth_tmax_raster, 'w', **profile) as out:
            for b in range(0, profile['count']):
                arr = values.astype(rasterio.float32).reshape(shp)
                out.write(arr, b + 1)

    @classmethod
    def tearDownClass(cls):
        for path in cls._output_paths:
            if os.path.exists(path):
                os.remove(path)
        for prefix in ('aet', 'melt', 'pond', 'swe', 'sm'):
            for path in glob.glob(
                    os.path.join(TEST_DATA_DIR, '%s_????????.tif' % prefix)):
                os.remove(path)
        for prefix in ('pond', 'sm', 'soils', 'swe'):
            os.remove(os.path.join(TEST_DATA_DIR, '%s.pickled' % prefix))
        other_paths = list(map(lambda p: os.path.join(TEST_DATA_DIR, p), (
            'groundwater.json', 'streamflows.json', 'streamflows.pickled',
            'upper_soil.json', 'lateral_inflows.pickled')))
        for path in other_paths:
            os.remove(path)

    def test_full_hydrology_model_run(self):
        'Should execute a basic hydrology model run'
        # Define the Shapefile sources of the sub-basin and stream network
        #   geometries and attributes
        dataset = ModelVectorDatasets(
            fn_subsheds = os.path.join(TEST_DATA_DIR, 'subbasins.shp'),
            fn_network = os.path.join(TEST_DATA_DIR, 'rivers.shp'),
            verbose = False)
        # Generate list of dictionaries with default HBV parameters and
        #   write new dataset
        params_hbv = []
        for sub in dataset.subsheds.read_features():
            params_hbv.append({
                'SUBBASIN': sub['properties']['SUBBASIN'], 'hbv_ck0': 12,
                'hbv_ck1': 50, 'hbv_ck2': 10000, 'hbv_hl1': 50, 'hbv_perc': 2,
                'hbv_pbase': 5
            })
        # Generate list of dictionaries with default Muskingum routing
        #   parameters and write new dataset
        params_musk = []
        for riv in dataset.network.read_features():
            params_musk.append({
                'ARCID': riv['properties']['ARCID'], 'e': 0.3, 'ks': 86400
            })
        dataset.write_hbv_parameters(
            outfn = os.path.join(TEST_DATA_DIR, 'subbasins_with_params.shp'),
            params = params_hbv)
        dataset.write_muskingum_parameters(
            outfn = os.path.join(TEST_DATA_DIR, 'rivers_with_params.shp'),
            params = params_musk)
        # Define names of input parameter GeoTIFF files to be created
        fn_temp_thresh = os.path.join(TEST_DATA_DIR, self._output_hydro_rasters[0])
        fn_ddf = os.path.join(TEST_DATA_DIR, self._output_hydro_rasters[1])
        fn_soil_max_water = os.path.join(TEST_DATA_DIR, self._output_hydro_rasters[2])
        fn_soil_beta = os.path.join(TEST_DATA_DIR, self._output_hydro_rasters[3])
        fn_aet_lp_param = os.path.join(TEST_DATA_DIR, self._output_hydro_rasters[4])
        # Location of the output file, which will describe, for each parameter,
        #   the name of the raster file
        fn_json_param = os.path.join(TEST_DATA_DIR, 'param_files_test.json')
        raster_dataset = ModelRasterDatasetHBV(
            self._synth_precip_raster, fn_temp_thresh, fn_ddf, fn_soil_max_water,
            fn_soil_beta, fn_aet_lp_param)
        raster_dataset.write_parameter_to_geotiff(fn_temp_thresh, 2.0)
        raster_dataset.write_parameter_to_geotiff(fn_ddf, 2.5)
        raster_dataset.write_parameter_to_geotiff(fn_soil_max_water, 50.0)
        raster_dataset.write_parameter_to_geotiff(fn_soil_beta, 0.5)
        raster_dataset.write_parameter_to_geotiff(fn_aet_lp_param, 0.5)
        raster_dataset.write_parameter_input_file(fn_json_param)
        # Finally, run the model
        model = HydroEconModel()
        model.hydro(
            init_date = '20120901',
            precip_file = self._synth_precip_raster,
            tmin_file = self._synth_tmin_raster,
            tmax_file = self._synth_tmax_raster,
            params = os.path.join(TEST_DATA_DIR, 'param_files_test.json'),
            network_file = os.path.join(TEST_DATA_DIR, 'rivers_with_params.shp'),
            basin_file = os.path.join(TEST_DATA_DIR, 'subbasins_with_params.shp'),
            hydro_maps_of = TEST_DATA_DIR, hydro_ts_of = TEST_DATA_DIR,
            restart_file = TEST_DATA_DIR, verbose = False)

    def test_geotiff_output_files(self):
        'Test output GeoTIFF file contents'
        parameters = ('aet', 'sm', 'pond', 'swe', 'melt')
        geotiffs = [
            '%s_20120901.tif' % p for p in parameters
        ]
        geotiff_checksums = [
            'ff325f5c7c2e60b75a08194fcfb0cf6756438f09e434d0c0ad26306873cc0e64',
            '006662f93add0a2603c5a9981811b68e8a1d456aa86d249e7573e3698dc24bbe',
            'd4064b779685a775936796f3ec624fa9939aabb675e8edee460e5f62ea31cf4d',
            'cd36696a9f8fc6d9876c670f6914de7cc551d8ad39e6513d9a9f6ebad8aae3a5',
            'f994b3e4dabeb648384fb81e42c8ab7bbbe0847a64ea72df265d1507c2291a30',
        ]
        # Test that there are 3 output files for each parameter
        for prefix in parameters:
            self.assertTrue(len(
                glob.glob('%s/%s_2012*.tif' % (TEST_DATA_DIR, prefix))) == 3)
        for i, tif in enumerate(geotiffs):
            checksum = hashlib.sha256()
            with open(os.path.join(TEST_DATA_DIR, tif), 'rb') as file:
                checksum.update(file.read())
                self.assertEqual(checksum.hexdigest(), geotiff_checksums[i])

    def test_groundwater_and_upper_soil_output_files(self):
        'Test contents of output groundwater.json, upper_soil.json files'
        for filename in ('groundwater.json', 'upper_soil.json', 'streamflows.json'):
            with open(os.path.join(TEST_DATA_DIR, filename), 'r') as file:
                data = json.load(file)
                self.assertTrue('nodes' in data.keys())
                for node in data['nodes']:
                    self.assertTrue('id' in node.keys())
                    self.assertTrue('dates' in node.keys())
                    self.assertTrue('units' in node.keys())
                    if node['id'] != 172:
                        self.assertTrue(node['dates'][0]['flow'] == 0.0)
                    elif filename == 'streamflows.json':
                        self.assertTrue(round(node['dates'][0]['flow'], 3) == 0.002)
                    else:
                        self.assertTrue(round(node['dates'][0]['flow'], 3) == 0.119)

    def test_streamflows_output_file(self):
        'Test contents of output streamflows.pickled file'
        with open(os.path.join(TEST_DATA_DIR, 'streamflows.pickled'), 'rb') as file:
            data = pickle.load(file)

        self.assertEqual(data.size, 5)
        self.assertEqual(data.mean().round(5), 0.0262)

    def test_streamflows_output_file(self):
        'Test contents of output lateral_inflows.pickled file'
        with open(os.path.join(TEST_DATA_DIR, 'lateral_inflows.pickled'), 'rb') as file:
            data = pickle.load(file)

        self.assertEqual(data.size, 5)
        self.assertEqual(data.mean().round(5), 0.0435)


if __name__ == '__main__':
    unittest.main()
