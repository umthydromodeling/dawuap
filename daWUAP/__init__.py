__all__ = ["hydroengine", "utils", "hydrovehicle", "econengine", "tests", "assimilation"]
__version__ = "0.2.0.dev"

SUBSHED_ID = 'SUBBASIN'
NETWORK_ID = 'ARCID'
