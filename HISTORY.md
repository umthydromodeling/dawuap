# HISTORY

## 2020-10-07

- hyena.py will check that the land-use raster provided has the same shape as the input climate datasets. 
- daWUAP.econengine.econfuncs.Farm now has a "verbose" mode that will suppress print() statements, e.g., when running tests.
- Updated daWUAP.utils.check_farms, fixing check_farms() so that it works for list, dict of lists, and dict input types.

## 2020-09-17

- daWUAP.utils now has explicit sub-modules, e.g., daWUAP.utils.raster, daWUAP.utils.vector, etc.
- Added daWUAP.utils.fixtures module with various generate_*() functions for creating template Farm, Observation, and Scenario data dictionaries.
